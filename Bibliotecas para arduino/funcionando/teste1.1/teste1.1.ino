 /*
###################################
### Luiz Gustavo e Lukas Lorenz ###
###################################

 Arduino pin
  4 -> H0X711 CLK
  2 -> DT
  5V -> VCC
  GND -> GND
 
 LCD:
  Vss (1) -> GND
  VDD     -> 5V
  V0      -> Pino central do potenciometro
  RS      -> 13
  RW      -> GND
  E       -> 12
  D4      -> 11
  D5      -> 10
  D6      -> 9
  D7      -> 8
  A       -> 5V
  K (16)  -> GND
*/

#include "HX711.h"
#include <LiquidCrystal.h>
 
#define calibration_factor -7050.0 //This value is obtained using the SparkFun_HX711_Calibration sketch
#define DOUT  2 // 3,5,6
#define CLK  4

HX711 scale(DOUT, CLK);
LiquidCrystal lcd(13,12, 11, 10, 9,8);
float weight = 0;

void printLCD(float weight){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("SD: ");
  lcd.print(weight);
  lcd.print("  kg"); 
}
void setup(){
  lcd.begin(16, 2);
  Serial.begin(9600);
  Serial.println("HX711 scale demo");

  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0

  lcd.setCursor(4, 0);
  lcd.print("LOADING...");
  delay(500);
}

void loop() {
  weight = scale.get_units()*0.453592;
  
  Serial.print(weight, 1 ); //scale.get_units() returns a float
  Serial.print(" kg"); 
  Serial.println();
  
  printLCD(weight);
  
  delay(500);
}
