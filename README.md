***INTEGRANTES***

- Luiz Gustavo

- Lukas Lorenz

- Renan Sisnando

# Objetivo:

Elaboração de uma balança para mensurar o peso do fórmula AF17;

# Relatório:

https://www.overleaf.com/13511745xcwjckkzkszk

# Reuniões:

- 16/01:

-> Pauta: Separar tarefas, definição do projeto, relatório.

-> Definição do projeto: uma plataforma genérica conectada ao Arduíno Uno Standalone, 
de modo que seja possível conectar as outras três plataformas ao mesmo micro controlador. Caso seja feito quatro plataformas, 
colocar ganchos ou presilhas para prender na tilt table pelo seu interior;

-> Flexibilidades no projeto: possibilidade de usar jacks, fonte de energia na tomada ou bateria;

-> Definir materiais e componentes elétricos;

-> Dimensões do carro: 2100 x 1500 mm, roda Hozier (225 x d515 mm), roda na Carrera (200 x d515 mm) , distância entre eixos 1040 mm, distância entre rodas (interna) 1100 mm.

-> Lembrete: abrir e estudar as balanças da salinha, tentar conserta-las usando o HX711 (trabalhar em paralelo com o projeto da balança principal)

- 17/01:

-> Hangout com Renan (Reunião);

-> Definiu-se a estrutura final da plataforma;

-> Projeto em paralelo da construção da balança;

- 24/01:

-> Definição final dos materiais utilizados para a plataforma da balança e duas opções de material para a central, conforme a tabela abaixo:

    Elemento          | Material    | Espessura (mm)
    ------------------|-------------|---------------
    Base              | MDF         |   2~5
    Tampa             | MDF         |   10~8
    Laterais          | MDF         |   2
    Suportes internos | MDF         |   1~2
    Central           | Acrílico/3D |   1~2
    Pés               | Borracha    |   10
    

-> Uso do Nobreak como auxílio para fonte alternativa como no tutorial: https://www.youtube.com/watch?v=dZp8s63rCD8

-> Alimentação com fonte e bateria "bolacha";
    
-> Dilema sobre o LCD, 20X4 ou uma matriz maior e um pouco mais cara;

- 30/01:

-> Devido aos materiais disponíveis no bloco, tem-se que:

Definição final dos materiais utilizados para a plataforma da balança e duas opções de material para a plataforma, conforme a tabela abaixo:

    Elemento                    | Material    | Espessura (mm)
    ----------------------------|-------------|---------------
    Tampa                       | MDF         |   7
    Base                        | MDF         |   22~25
    Base                        | Vidro       |   10~15
    Laterais                    | MDF         |   7
    Suportes internos           | MDF         |   1~2
    Central                     | Acrílico/3D |   1~2
    Pés (plataforma triangular  | Borracha    |   10
    
-> Começou-se orçamento;

-> Definiu-se, por fim, as espessuras e o orçamento total;

-> Opção de 3 pés com uma plataforma de borracha triângular;

-> Usar um fusivel e definir sua aperagem;

Tabela de Materias e orçamento da balança:

Material                          | Preço Unitário(R$)   |         Quantidade    |    Onde Encontrar  |  Já se tem
----------------------------------|----------------------|-----------------------|--------------------|-------------
Chapa de MDF(20mm de espessura)   |79,90                 |1                      |Leroy Merlim        |
Hx711                             |10,12                 |1                      |Baú da Eletrônica   |
Capacitor(10uF)                   |0,08                  |2                      |Hu Infinito         |
Capacitor(22pF)                   |0,05                  |2                      |Hu Infinito         |
LED                               |0,13                  |1                      |Hu Infinito         |ok
Potenciômetro(10k)                |1,15                  |1                      |Hu Infinito         |ok
Bateria(9V)                       |9,50                  |1                      |Hu Infinito         |
Cristal(16MHz)                    |0,56                  |1                      |Hu Infinito         |
Fusível(1A, 2A, 3A)               |0,16                  |1                      |Hu Infinito         |
Oscilador 7805                    |1,28                  |1                      |Baú da Eletrônica   |
LCD 20X4                          |33,90                 |1                      |Hu Infinito         |
LCD 16X2                          |13,90                 |1                      |Hu Infinito         |ok
Resistor(220Ohm)                  |0,12                  |1                      |Hu Infinito         |
Conector USB fêmea tipo A         |1,45                  |8                      |Hu Infinito         |
Conector USB macho tipo A         |1,66                  |8                      |Hu Infinito         |
Botão                             |0,88                  |1                      |Hu Infinito         |ok
Fio                               |                      |                       |                    |
Parafuso                          |                      |                       |                    |
ATmega328                         |16,60                 |1                      |Hu Infinito         |ok


# ***Dimensões da PCB ***

- 10.6 x 10 mm






