# Links:

Integração Arduino - Matlab


- https://www.youtube.com/watch?v=SiaUzO72tqE&list=PLi2hbezQRVS0cPMeW3uDlUHnO_rPvJCV9&index=1
 
- https://www.youtube.com/watch?v=bB5-mvA41cI&list=PLnFVwvy9GeM4Z994R8sbDLFE9yj8Ad9KN

- https://www.youtube.com/watch?v=udIyzonx9-A

- https://www.youtube.com/watch?v=Vz8ESnfj_Jw


Exemplos de projetos similares já feitos

- http://www.instructables.com/id/Make-your-weighing-scale-hack-using-arduino/

- http://www.instructables.com/id/Arduino-Scale-With-5kg-Load-Cell-and-HX711-Amplifi/

- http://blog.usinainfo.com.br/sensor-de-peso-com-arduino-variacao-de-um-strain-gage/

- https://www.youtube.com/watch?v=QgTkORAG81w

Links sobre o uso do usb

- https://www.youtube.com/watch?v=ClD5Y4z_nJc

- http://www.electroschematics.com/4856/usb-how-things-work/

- http://www.instructables.com/id/Creating-you-own-USB-cables/

- http://www.us-electronics.com/files/usbconnectors.pdf